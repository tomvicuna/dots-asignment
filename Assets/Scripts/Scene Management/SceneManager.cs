﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager<TTransitionData> {

	//the scene manager has two basic functions:
	// 1. managing the stack of scenes
	// 2. Fowarding the events to the current scene

	internal GameObject SceneRoot { get; set; }

	//the active scenes in the game are organized in a stack representing a history of scenes...
	private static readonly Stack<Scene<TTransitionData>> _sceneStack = new Stack<Scene<TTransitionData>>();

	//the top of the stack is the current scene
	public static Scene<TTransitionData> CurrentScene { 
		get {
			return _sceneStack.Count !=0 ? _sceneStack.Peek() : null;
		}
	}

	public void PopScene(){
		Scene<TTransitionData> previousScene = null;
		Scene<TTransitionData> nextScene = null;

		if(_sceneStack.Count != 0){
			previousScene = _sceneStack.Peek();
			_sceneStack.Pop();
		}
		if(_sceneStack.Count != 0){
			nextScene = _sceneStack.Peek();
		}

		if(previousScene != null){
			previousScene._OnExit(nextScene);

			GameObject.Destroy(previousScene.gameObject);
		}

		if(nextScene != null){
			nextScene._OnEnter(previousScene);
		}
	}

	public void PushScene<T> () where T : Scene<TTransitionData> {
		var previousScene = CurrentScene;
		var nextScene = CreateScene<T>();

		_sceneStack.Push(nextScene);
		nextScene._OnEnter(previousScene);

		if(previousScene != null){
			previousScene._OnExit(nextScene);
		}
	}

	public void SwapScene<T> () where T : Scene<TTransitionData> {
		Scene<TTransitionData> previousScene = null;

		if(_sceneStack.Count !=0){
			previousScene = _sceneStack.Peek();
			_sceneStack.Pop();
		}
		var nextScene = CreateScene<T>();
		_sceneStack.Push(nextScene);
		nextScene._OnEnter(previousScene);

		if(previousScene != null){
			previousScene._OnExit(nextScene);
			GameObject.Destroy(previousScene.gameObject);
		}
	}

	private T CreateScene<T>() where T : Scene<TTransitionData>{
		var sceneObject = new GameObject {name = typeof (T).Name}; 
		sceneObject.transform.SetParent(SceneRoot.transform); 
		return sceneObject.AddComponent<T>();
	}
}
