﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteGameScene : Scene<GameData> {
	
	GameBoard gameBoard;

	internal override void OnEnter (Scene<GameData> previousScene)
	{
		gameBoard = new GameObject("BoardHolder").AddComponent<GameBoard>();
		gameBoard.Init(GameSettings.Instance.GRID_COLLUMS, GameSettings.Instance.GRID_ROWS);
	}

	void Update(){
		if(GameManager.Controller.InputPressed){
			Vector2 worldPosInput = GameManager.Controller.GetInputWorldPosition();
			gameBoard.UpdateBoard(worldPosInput);
		}
		else if(GameManager.Controller.InputRelease){
			gameBoard.TryCashingCollection();
		}

		if(Input.GetKeyDown(KeyCode.R)){
			GameManager.SceneManager.SwapScene<InfiniteGameScene>();
		}
	}

	internal override void OnExit (Scene<GameData> nextScene)
	{
		Destroy(gameBoard.gameObject);
	}
}
