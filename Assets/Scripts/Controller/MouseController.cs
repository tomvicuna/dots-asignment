﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : Controller {

	Vector3 mouseWorldPos {get { return Camera.main.ScreenToWorldPoint (Input.mousePosition) ;} }

	public Vector3 startPos;
	public Vector3 releasePos;

	public override bool InputPressed{ get { return Input.GetMouseButton(0) && !Input.GetMouseButtonDown(0)  ; } }
	public override bool InputRelease{ get { return Input.GetMouseButtonUp(0); } }

	public override Vector2 GetInputWorldPosition(){
		return mouseWorldPos;
	}
}
