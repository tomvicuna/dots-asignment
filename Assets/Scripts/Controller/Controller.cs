﻿using UnityEngine;

public abstract class Controller {

	public abstract bool InputPressed{get;}

	public abstract bool InputRelease{get;}

	public abstract Vector2 GetInputWorldPosition();
}
