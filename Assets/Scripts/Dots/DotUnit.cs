﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DotUnit : MonoBehaviour {

	public bool IsActive {get { return gameObject.activeSelf;} }
	public bool IsSelected;

	public bool ShouldExplode {get; private set;}

	public bool IsBusy{ get { return coroutine != null ;} }

	Coroutine coroutine;

	Animator animator;

	public Vector3 targetPosition;

	float movingSpeed = .4f;

	public bool IsOnPosition{ get { return CloseEnough(transform.localPosition, targetPosition) ; }}

	public Transform parent{
		get{ return transform.parent; }
		set{ transform.SetParent(value); }
	}

	public Color ColorType {get; private set;}
	SpriteRenderer spriteRenderer;
	DotManager dotManager;

	public void Init(DotManager dotManager){
		this.spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		this.dotManager = dotManager;
		this.ShouldExplode = false;
		animator = GetComponent<Animator>();
		RandomizeColor();
	}

	public void Activate(){
		gameObject.SetActive(true);
	}

	public void MarkAsInsidePolygon(){
		ShouldExplode = true;
		spriteRenderer.color = Color.black;
		gameObject.name = "BOMB";
		if(!IsSelected){
			Select();
		}
	}

	public void Deactivate(){
		IsSelected = false;
		gameObject.SetActive(false);
		gameObject.name = "none";
		transform.localScale = Vector3.one;
	}

	public void RandomizeColor(){
		ColorType = dotManager.GetNewColor();
		spriteRenderer.color = GetColor();
	}

	public void Select(){
		IsSelected = true;
		StopCurrentWork();
		coroutine  = StartCoroutine(SelectAnimation());
	}

	public void Deselect(){
		IsSelected = false;
		spriteRenderer.color = GetColor();
		StopCurrentWork();
		coroutine = StartCoroutine(DeselectAnimation());
	}

	void StopCurrentWork(){
		if(coroutine != null)
			StopCoroutine(coroutine);
		coroutine = null;
	}

	public Color GetColor(){
		return ColorType;
	}
	public void Pop(){
		StopCurrentWork();
		coroutine = StartCoroutine (PopAnimation() );
	}

	public void MoveTowardsTarget(){
		coroutine = StartCoroutine( MoveToTarget() );
	}


	IEnumerator MoveToTarget(){
		while(!IsOnPosition){
			transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPosition, movingSpeed);
			yield return null;
		}
		transform.localPosition = targetPosition;
		animator.SetTrigger("Bounce");
		coroutine = null;
	}

	bool CloseEnough(Vector3 a, Vector3 b){
		return CloseEnough(a.x, b.x) && CloseEnough(a.y, b.y);
	}

	bool CloseEnough(float a, float b){
		return Mathf.Abs(a-b) < .01f;
	}

	IEnumerator PopAnimation(){
		Vector3 newSize = transform.localScale;
		while(newSize.x > 0){
			newSize.x -= .1f;
			newSize.y -= .1f;
			transform.localScale = newSize;
			yield return null;
		}
		coroutine = null;
		dotManager.DestroyDot(this);
	}

	IEnumerator SelectAnimation(){
		Vector3 newSize = transform.localScale;
		while(newSize.x < 1.5f){
			newSize.x += .05f;
			newSize.y += .05f;
			transform.localScale = newSize;
			yield return null;
		}
		transform.localScale = new Vector3(1.5f,1.5f,1.5f);
		coroutine = null;
	}

	IEnumerator DeselectAnimation(){
		Vector3 newSize = transform.localScale;
		while(newSize.x > 1){
			newSize.x -= .05f;
			newSize.y -= .05f;
			transform.localScale = newSize;
			yield return null;
		}
		transform.localScale = Vector3.one;
		coroutine = null;
	}
}