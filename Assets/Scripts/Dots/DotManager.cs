﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DotManager
{

    public GameObject dotPrejab;

    List<DotUnit> activeDots = new List<DotUnit>();
    ShuffleBag<Color> colors;

    static DotManager _instance;
    public static DotManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new DotManager();
            }
            return _instance;
        }
    }

    DotManager()
    {
        InitColors();
        dotPrejab = Resources.Load("Prefabs/Dot") as GameObject;
    }

    public DotUnit GetNewDot()
    {
        GameObject instance = UnityEngine.Object.Instantiate(dotPrejab, Vector3.zero, Quaternion.identity) as GameObject;
        DotUnit dot = instance.GetComponent<DotUnit>();
        dot.Init(this);
        dot.RandomizeColor();
        dot.Activate();
        activeDots.Add(dot);
        return dot;
    }

    public void DeselectCollection(List<DotUnit> collection)
    {
        foreach (DotUnit dot in collection)
        {
            dot.Deselect();
        }
    }

    public void DestroyDot(DotUnit dot)
    {
        activeDots.Remove(dot);
        UnityEngine.Object.Destroy(dot.gameObject);

    }

    void InitColors()
    {
        Color[] settingColors = GameSettings.Instance.DOT_COLORS;
        colors = new ShuffleBag<Color>(settingColors.Length);
        foreach (Color color in settingColors)
        {
            colors.Add(color, 3);
        }
        //colors.Add(settingColors[0]);
    }

    public Color GetNewColor()
    {
        return colors.Next();
    }
}
