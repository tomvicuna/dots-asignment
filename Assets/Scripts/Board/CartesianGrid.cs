﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartesianGrid<T> where T : Object {

	public readonly List<CartesianCoordinate> AvailableCoordinates;
	public List<T> Data{
		get{
			List<T> dots = new List<T>();
			foreach(T dot in gridOfDots){
				dots.Add(dot);
			}
			return dots;
		}
	}
	public int columns;
	public int rows;
	T[,] gridOfDots;

	public CartesianGrid(int columns, int rows) {
		gridOfDots = new T[columns,rows];
		AvailableCoordinates = new List<CartesianCoordinate>();
		this.columns = columns;
		this.rows = rows;
		for(int x = 0 ; x < columns; x++){
			for(int y = 0; y < rows; y++){
				CartesianCoordinate coordinate = new CartesianCoordinate(x,y);
				AvailableCoordinates.Add(coordinate);
			}
		}
	}

	public bool ShouldDropOnePlace(CartesianCoordinate cartesian){
		return !IsPositionEmpty(cartesian) 
			&& IsPositionEmpty(new CartesianCoordinate(cartesian.x, cartesian.y - 1));
	}

	public bool IsPositionEmpty(CartesianCoordinate gridCoordinate){
		return (gridOfDots[gridCoordinate.x,gridCoordinate.y] == null);
	}

	public void AssignElementAt(T dot ,CartesianCoordinate cartesian){
		gridOfDots[cartesian.x,cartesian.y] = dot;
	}
	
	public T GetElementAt(CartesianCoordinate cartesian){
		return gridOfDots[cartesian.x,cartesian.y];
	}

	public void RemoveElementAt(CartesianCoordinate cartesian){
		gridOfDots[cartesian.x,cartesian.y] = null;
	}

	public List<T> GetTheSurroundingElements(CartesianCoordinate c){
		List<T> surroundingDots = new List<T>();
		if(c.x < columns)
			surroundingDots.Add( gridOfDots[c.x + 1, c.y] );
		if(c.x > 0) 
			surroundingDots.Add( gridOfDots[c.x - 1, c.y] );
		if(c.y < rows) 
			surroundingDots.Add( gridOfDots[c.x, c.y + 1] );
		if(c.y > 0) 
			surroundingDots.Add( gridOfDots[c.x, c.y - 1] );

		return surroundingDots;
	}

	public CartesianCoordinate LowerElementAt(CartesianCoordinate cartesian){
		gridOfDots[cartesian.x,cartesian.y - 1] = gridOfDots[cartesian.x,cartesian.y];
		gridOfDots[cartesian.x,cartesian.y] = null;
		cartesian.y -= 1;
		return cartesian;
	}

	bool IsValidCoordinate(CartesianCoordinate c){
		return c.x >= 0 && c.x < columns && c.y > 0 && c.y < rows;
	}
}

public struct CartesianCoordinate{
	public int x;
	public int y;
	public CartesianCoordinate(int x, int y){
		this.x = x;
		this.y = y;
	}
}