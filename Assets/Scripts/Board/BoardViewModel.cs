﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardViewModel {

	GameBoard parentBoard;
	public int columns;
	public int rows;

	int[] topSpawnBuffer;

	float SEPARATION_DISTANCE;
	// Use this for initialization
	public BoardViewModel (GameBoard root) {
		parentBoard = root;
		columns = parentBoard.columns;
		rows = parentBoard.rows;
		topSpawnBuffer = new int[columns];
		SEPARATION_DISTANCE = GameSettings.Instance.DOT_SEPARATION;
		ResetTopSpawnBuffer();
	}
	
	public void CenterGrid(){
		float offsetY = rows > 1 ? ( (float) rows - 1) * SEPARATION_DISTANCE * -.5f : 1;
		float offSetX = columns > 1 ? ( (float) columns - 1) * SEPARATION_DISTANCE * -.5f : 1;
		parentBoard.transform.position = new Vector3( offSetX , offsetY ,0f);
	}

	public void PositionDotAt(DotUnit dot, CartesianCoordinate cartesian){
		PositionDotAt(dot, CartesianToWorldPosition(cartesian));
	}

	public void PositionDotAt(DotUnit dot, Vector3 newPosition){
		dot.parent = parentBoard.transform;
		dot.gameObject.transform.localPosition = newPosition;
		dot.targetPosition = newPosition;
		dot.Activate();
	}

	public void PositionAboveWithBuffer(DotUnit dot, int x){
		dot.parent = parentBoard.transform;
		dot.gameObject.transform.localPosition = CartesianToWorldPosition(x, (rows - 1) + topSpawnBuffer[x]);
		dot.targetPosition = CartesianToWorldPosition(x, (rows - 1));
		topSpawnBuffer[x]++;
		dot.Activate();
	}

	public void ResetTopSpawnBuffer(){
		for(int i = 0; i <  topSpawnBuffer.Length; i++){
			topSpawnBuffer[i] = 1;
		}
	}

	public CartesianCoordinate WorldPositionToCartesian(Vector3 position){
		return new CartesianCoordinate(Mathf.RoundToInt(position.x / SEPARATION_DISTANCE) , Mathf.RoundToInt (position.y / SEPARATION_DISTANCE));
	}

	public Vector3 CartesianToWorldPosition(CartesianCoordinate gridCoordinate){
		return new Vector3(gridCoordinate.x * SEPARATION_DISTANCE , gridCoordinate.y * SEPARATION_DISTANCE ,0f);
	}

	public Vector3 CartesianToWorldPosition(int x, int y){
		return new Vector3(x * SEPARATION_DISTANCE , y * SEPARATION_DISTANCE ,0f);
	}
}
