﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailPainter {

	Stack<Vector3> trailPositions;

	LineRenderer trailRenderer;
	LineRenderer headRenderer;

	GameObject trailObject;

	float lineWidth = .15f;

	public TrailPainter () {
		this.trailObject = new GameObject("TrailPainter");
		this.trailRenderer = CreateLineRenderer("Body");
		this.headRenderer = CreateLineRenderer("Head");
		this.headRenderer.numCapVertices = 90;
		headRenderer.positionCount = 0;
		this.trailPositions = new Stack<Vector3>();
	}

	LineRenderer CreateLineRenderer(string name){
		GameObject holder = new GameObject(name);
		LineRenderer renderer = holder.AddComponent<LineRenderer>();

		holder.transform.SetParent(trailObject.transform);
		renderer.material = new Material(Shader.Find("Unlit/Color"));
		renderer.material.color = Vector4.zero;
		renderer.widthMultiplier = lineWidth;

		return renderer;
	}

	public void Refresh(Vector3 currentPos){
		trailRenderer.positionCount = trailPositions.Count;
		trailRenderer.SetPositions(trailPositions.ToArray());

		if(trailPositions.Count > 0){
			Vector3[] head = new Vector3[] { trailPositions.Peek(), currentPos };
			headRenderer.positionCount = 2;
			headRenderer.SetPositions(head);
		}
		else{
			headRenderer.positionCount = 0;
		}
	}

	public void AddToTrail(DotUnit dot){
		Vector3 edge = dot.gameObject.transform.position;
		edge.z = 0;
		SetColor(dot.GetColor());
		trailPositions.Push(edge);
	}

	void SetColor(Color col){

		trailRenderer.startColor = col;
		trailRenderer.endColor = col;
		trailRenderer.material.SetColor("_Color", col);

		headRenderer.startColor = col;
		headRenderer.endColor = col;
		headRenderer.material.SetColor("_Color", col);
	}

	public void UndoLastEdge(){
		if(trailPositions.Count > 0)
			trailPositions.Pop();
	}

	public void Clear(){
		trailPositions.Clear();
		Refresh(Vector3.zero);
	}

	public void CleanUp(){
		GameObject.Destroy(trailObject);
	}
}
