﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotCollector {
	Stack<DotUnit> collectedDots = new Stack<DotUnit>();
	List<DotUnit> bonusCollection = new List<DotUnit>();

	DotUnit lastDotSelected {get { return collectedDots.Peek();} }
	DotUnit previousDotSelected;
	DotUnit newCandidate;
	GameBoard gameBoard;

	bool hasPolygon;

	TrailPainter trailPainter;

	public int Collected { get { return collectedDots.Count; } }

	public bool CanCollect { get {return collectedDots.Count > 1 ;} }

	bool IsCollectionEmpty { get { return collectedDots.Count == 0; } }

	public DotCollector(GameBoard gameBoard){
		this.gameBoard = gameBoard;
		this.trailPainter = new TrailPainter();
	}

	public bool DoesCollectionFormAPolygon(){ return hasPolygon; }

	public void UpdateVisualTrail(Vector2 worldPoint){ 
		trailPainter.Refresh(worldPoint); 
	}

	public void TryCollect(DotUnit dot){

		if(newCandidate == dot)
			return;

		newCandidate = dot;

		if(IsCollectionEmpty){
			AddToCollection();
		}
		else if(IsNewDotPreviousToLast()){
			GoBackOneStep();
		}
		else if( IsCandidateANeighbour() && IsCandidateSameAsCollection() && !hasPolygon){
			if(IsCandidateClosingAPolygon()){
				HandleSquareMechanic();
			}
			AddToCollection();
		}
	}

	void AddToCollection(){
		if(!IsCollectionEmpty)
			previousDotSelected = collectedDots.Peek();
		newCandidate.Select();
		collectedDots.Push(newCandidate);
		trailPainter.AddToTrail(newCandidate);
	}

	bool IsNewDotPreviousToLast(){
		return !IsCollectionEmpty && previousDotSelected == newCandidate;
	}

	void GoBackOneStep(){

		if(!hasPolygon){
			lastDotSelected.Deselect();
		}
		collectedDots.Pop();

		if(collectedDots.Count > 1){
			DotUnit du = collectedDots.Pop();
			previousDotSelected = collectedDots.Peek();
			collectedDots.Push(du);
		}
		else{
			previousDotSelected = null;
		}
		if(hasPolygon){
			List<DotUnit> bombs = gameBoard.GetBombs();
			foreach(DotUnit bomb in bombs){
				bonusCollection.Add(bomb);
			}

			DotManager.Instance.DeselectCollection(bonusCollection);

			bonusCollection.Clear();
			hasPolygon = false;
		}

		trailPainter.UndoLastEdge();
	}

	bool IsCandidateANeighbour(){
		return gameBoard.AreTheseDotsNeighbours(lastDotSelected, newCandidate);
	}

	bool IsCandidateSameAsCollection(){
		return lastDotSelected.ColorType == newCandidate.ColorType;
	}

	bool IsCandidateClosingAPolygon(){
		foreach(DotUnit dot in collectedDots){
			if (newCandidate == dot){
				return true;
			}
		}
		return false;
	}

	void HandleSquareMechanic(){
		hasPolygon = true;
		List<DotUnit> allSameColor = gameBoard.GetAllDotsOfSameColor(newCandidate.ColorType);
		foreach(DotUnit sameColorDot in allSameColor){
			if(!collectedDots.Contains(sameColorDot)){
				sameColorDot.Select();
				bonusCollection.Add(sameColorDot);
			}
		}
	}

	public List<DotUnit> GetDotsAvailableToCashIn(){
		List<DotUnit> collection = new List<DotUnit>();

		if(hasPolygon){
			foreach(DotUnit dot in bonusCollection){
				if(!dot.ShouldExplode)
					collection.Add(dot);
			}
		}
		foreach(DotUnit dot in collectedDots){
			if(!collection.Contains(dot))
				collection.Add(dot);
		}
		return collection;
	}

	public List<DotUnit> GetOnlyDotsThatFormPolygon(){
		List<DotUnit> dotsFromPolygon = new List<DotUnit>();
		Queue<DotUnit> entireCollection = new Queue<DotUnit>(collectedDots);;
		if(hasPolygon){
			while(entireCollection.Count != 0){
				DotUnit dot = entireCollection.Dequeue();
				if(!dotsFromPolygon.Contains(dot)){
					dotsFromPolygon.Add(dot);
				}
				else{
					break;
				}
			}
		}
		return dotsFromPolygon;
	}

	public void DeselectAll(){
		foreach(DotUnit dot in collectedDots){
			dot.Deselect();
		}
	}

	public void Reset(){
		trailPainter.Clear();
		collectedDots.Clear();
		bonusCollection.Clear();
		newCandidate = null;
		previousDotSelected = null;
		hasPolygon = false;
	}

	public void CleanUp(){
		trailPainter.CleanUp();
	}
}
