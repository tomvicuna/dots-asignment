﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoard : MonoBehaviour {

	public int columns;
	public int rows;

	CartesianGrid<DotUnit> boardGrid;
	BoardViewModel viewModel;
	DotCollector dotCollector;

	public bool CanPlay {get; private set;}

	public void Init(int columns, int rows){
		this.columns = columns;
		this.rows = rows;
		this.boardGrid = new CartesianGrid<DotUnit>(columns,rows);
		this.dotCollector = new DotCollector(this);
		this.viewModel = new BoardViewModel(this);
		PopulateBoard();
		viewModel.CenterGrid();
		CanPlay = true;
	}

	void PopulateBoard(){
		for(int x = 0; x < columns; x++){
			for(int y = 0; y < rows; y++){
				PullDotFromBankAndPositionAt(x,y);
			}
		}
	}
	void PullDotFromBankAndPositionAt(int x, int y){
		DotUnit dot = DotManager.Instance.GetNewDot();
		CartesianCoordinate cc = new CartesianCoordinate(x,y);
		viewModel.PositionDotAt(dot, cc);
		boardGrid.AssignElementAt(dot, cc);
	}

	public void UpdateBoard(Vector2 input){
		if(!CanPlay) return;

		dotCollector.UpdateVisualTrail(input);

		RaycastHit2D hit = Physics2D.Raycast(input, Vector2.zero);

		if(hit.collider != null && hit.collider.tag == "Dot"){
			DotUnit dot = hit.collider.gameObject.GetComponent<DotUnit>();
			dotCollector.TryCollect(dot);

			if(dotCollector.DoesCollectionFormAPolygon()){
				MarkDotsInsidePolygon(dotCollector.GetOnlyDotsThatFormPolygon());
			}
		}
	}

	void MarkDotsInsidePolygon(List<DotUnit> collectedDots){
		Polygon polygon = new Polygon();

		foreach(DotUnit dot in collectedDots){
			CartesianCoordinate vertice = viewModel.WorldPositionToCartesian(dot.targetPosition);
			polygon.AddVertice(vertice);
		}

		foreach(CartesianCoordinate cc in boardGrid.AvailableCoordinates){
			if(polygon.IsPointInsideArea(cc)){
				boardGrid.GetElementAt(cc).MarkAsInsidePolygon();
			}
		}
	}

	public void TryCashingCollection(){
		if(dotCollector.CanCollect){
			List<DotUnit> collectedDots = dotCollector.GetDotsAvailableToCashIn();
			foreach(DotUnit dot in collectedDots){
				CartesianCoordinate cc = viewModel.WorldPositionToCartesian(dot.targetPosition);
				boardGrid.RemoveElementAt(cc);
			}
			StartCoroutine( PopCollectionAndRearrangeBoard(collectedDots) );
		}
		else{
			dotCollector.DeselectAll();
		}
		dotCollector.Reset();
	}

	IEnumerator PopCollectionAndRearrangeBoard(List<DotUnit> collectedDots){
		CanPlay = false;
		yield return PopUpCollection(collectedDots);
		DropDotsWhereSpaceAvailable();
		yield return MoveRemainingDotsTowardsNewTarget();
		yield return new WaitForSeconds(.2f);
		yield return ExplodeDotsInsidePolygon();
		DropDotsWhereSpaceAvailable();
		yield return MoveRemainingDotsTowardsNewTarget();
		viewModel.ResetTopSpawnBuffer();
		CanPlay = true;
	}

	IEnumerator PopUpCollection(List<DotUnit> collectedDots){
		foreach(DotUnit dot in collectedDots){
			dot.Pop();
		}
		foreach(DotUnit dot in collectedDots){
			while(dot != null){
				yield return null;
			}
		}
	}

	void DropDotsWhereSpaceAvailable(){
		bool anyMoved = false;
		for(int x = 0 ; x < columns; x++){
			for(int y = 1; y < rows; y++){
				CartesianCoordinate cc = new CartesianCoordinate(x,y);
				if( IsTopRow(y) && boardGrid.IsPositionEmpty(cc)){
					PullDotFromBankAndPositionAtColumn(x);
				}
				if ( boardGrid.ShouldDropOnePlace(cc)) {
					DropOnePositionAt(x,y);
					anyMoved = true;
				}
			}
		}
		if(anyMoved){
			DropDotsWhereSpaceAvailable();
		}
	}

	IEnumerator MoveRemainingDotsTowardsNewTarget(){
		foreach(DotUnit dot in boardGrid.Data){
			if(!dot.IsOnPosition)
				dot.MoveTowardsTarget();
		}
		foreach(DotUnit dot in boardGrid.Data){
			while(dot.IsBusy){
				yield return null;
			}
		}
	}

	IEnumerator ExplodeDotsInsidePolygon(){
		List<DotUnit> toBeDestroyed = new List<DotUnit>();
		List<DotUnit> bombs = GetBombs();

		foreach(DotUnit dot in bombs){
			CartesianCoordinate cc = viewModel.WorldPositionToCartesian(dot.targetPosition);
			List<DotUnit> neighbours = boardGrid.GetTheSurroundingElements(cc);
			neighbours.Add( dot );
			foreach(DotUnit neighbourDot in neighbours){
				if(!toBeDestroyed.Contains(neighbourDot))
					toBeDestroyed.Add(neighbourDot);
			}
		}
		yield return PopUpCollection(toBeDestroyed);
		yield return null;
	}

	void PullDotFromBankAndPositionAtColumn(int x){
		DotUnit dot = DotManager.Instance.GetNewDot();
		viewModel.PositionAboveWithBuffer(dot,x);
		boardGrid.AssignElementAt(dot, new CartesianCoordinate(x, rows - 1));
	}

	void DropOnePositionAt(int x, int y){
		CartesianCoordinate newCoordinate = boardGrid.LowerElementAt( new CartesianCoordinate(x,y));
		boardGrid.GetElementAt(newCoordinate).targetPosition = viewModel.CartesianToWorldPosition(new CartesianCoordinate(x,y - 1));
	}

	bool IsTopRow(int y){
		return y == rows - 1;
	}

	public bool AreTheseDotsNeighbours(DotUnit dotA, DotUnit dotB){
		float distanceX = Mathf.Abs(dotA.transform.position.x - dotB.transform.position.x);
		float distanceY = Mathf.Abs(dotA.transform.position.y - dotB.transform.position.y);
		CartesianCoordinate p = viewModel.WorldPositionToCartesian(new Vector3(distanceX,distanceY,0f));
		return (p.x == 1 && p.y == 0) || (p.x == 0 && p.y == 1);
	}

	public List<DotUnit> GetAllDotsOfSameColor(Color colorType){
		List<DotUnit> sameColor = new List<DotUnit>();
		foreach(DotUnit dot in boardGrid.Data){
			if(dot.IsActive && dot.ColorType == colorType){
				sameColor.Add(dot);
			}
		}
		return sameColor;
	}

	public List<DotUnit> GetBombs(){
		List<DotUnit> bombs = new List<DotUnit>();
		foreach(DotUnit dot in boardGrid.Data){
			if(dot.ShouldExplode){
				bombs.Add(dot);
			}
		}
		return bombs;
	}

	void OnDestroy(){
		dotCollector.CleanUp();
	}
}
