﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static Controller Controller = new MouseController();
	public static SceneManager<GameData> SceneManager = new SceneManager<GameData>();

	void Start () {
		SceneManager.SceneRoot = this.gameObject;
		SceneManager.PushScene<InfiniteGameScene>();
	}
}
