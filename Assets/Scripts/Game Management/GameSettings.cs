﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour {

	public static GameSettings Instance;

	public Color BACKGROUND_COLOR;

	public Color[] DOT_COLORS;

	public int GRID_COLLUMS = 8;
	public int GRID_ROWS = 8;

	public float DOT_SEPARATION = .8f;

	public int CAMERA_SIZE = 3;

	void Awake () {
		Instance = this;
		Camera.main.backgroundColor = BACKGROUND_COLOR;
		Camera.main.orthographicSize = CAMERA_SIZE;
	}
}
