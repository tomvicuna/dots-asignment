﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Polygon {
	List<CartesianCoordinate> vertices;
	public Polygon(){
		this.vertices = new List<CartesianCoordinate>();
	}

	public bool IsPointInsideArea(CartesianCoordinate toTest){
	   double angle = 0;
	   CartesianCoordinate p1,p2;

	   for (int i = 0;i < vertices.Count ;i++) {
			if(vertices[i].x == toTest.x && vertices[i].y == toTest.y){
				return false;
			} 
			p1.x = vertices[i].x - toTest.x;
			p1.y = vertices[i].y - toTest.y;
			p2.x = vertices[(i+1) % vertices.Count].x - toTest.x;
			p2.y = vertices[(i+1) % vertices.Count].y - toTest.y;
	      	angle += Angle2D(p1.x,p1.y,p2.x,p2.y);
	   }

	   if (System.Math.Abs(angle) < Mathf.PI)
	      return(false);
	   else
	      return(true);
	}

	public void AddVertice(CartesianCoordinate vertice){
		vertices.Add(vertice);
	}

/*
   Return the angle between two vectors on a plane
   The angle is from vector 1 to vector 2, positive anticlockwise
   The result is between -pi -> pi
*/
	double Angle2D(double x1, double y1, double x2, double y2){
		double dtheta,theta1,theta2;
		theta1 = System.Math.Atan2(y1,x1);
		theta2 = System.Math.Atan2(y2,x2);
		dtheta = theta2 - theta1;
		while (dtheta > Mathf.PI)
			dtheta -= Mathf.PI*2;
		while (dtheta < - Mathf.PI)
			dtheta += Mathf.PI*2;

		return(dtheta);
	}
}
